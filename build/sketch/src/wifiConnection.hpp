#line 1 "d:\\Repo\\magisterka\\src\\wifiConnection.hpp"
#ifndef __WIFICONNECTION_H__
#define __WIFICONNECTION_H__

#include "settings.hpp"
#include <stdint.h>
#include "WiFi.h"
#include <IPAddress.h>

class WifiConnection
{
private:
    static IPAddress ipaddress;
    static char *ssidList;


public:
    static void init();

    static uint8_t searchNetworks();
    static uint8_t connectToNetwork();

};

#endif