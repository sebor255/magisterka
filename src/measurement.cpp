#include "measurement.hpp" 

void measurePerm();

Task tMeasurePerm(TASK_IMMEDIATE, TASK_FOREVER, &measurePerm);


void measurePerm()
{   
    double value = Permeability::mesure();
    LCD::print_lower_line("Ind: " + patch::to_string(value));
    Serial.print("Inductance: ");
    Serial.println(value);
}

void Measurement::startMeasuring()
{   
    Serial.println("starting task forever: measurePerm");
    runner.addTask(tMeasurePerm);
    tMeasurePerm.enable();
}
