#include "webServer.hpp"
#include "patch.hpp"

const int8_t GENERAL_ERROR = -1; 
const int8_t NO_GET_METHOD = -2; 
const int8_t SUCCESS = 1; 


WebServer::WebServer(uint16_t portToListen)
{
    this->server = WiFiServer(portToListen);
    this->server.begin();
}

uint8_t WebServer::wait_for_client()
{
    this->client = server.available(); // Listen for incoming clients
}

void WebServer::check_request()
{   
    this->header = "";
    String currentLine = "";          // make a String to hold incoming data from the client
    while(this->client.connected()){
        char c = this->client.read();     // read a byte, then
        Serial.write(c);                  // print it out the serial monitor
        this->header += c;
        if (c == '\n') {                  // if the byte is a newline character
            // if the current line is blank, you got two newline characters in a row.
            // that's the end of the client HTTP request, so send a response:
            if (currentLine.length() == 0) {
                // do some actions on GET request
                this->GET_actions();
                // display html page
                //this->displayPage();
                break;
            } else { // if you got a newline, then clear currentLine
                currentLine = "";
            }
        }else if (c != '\r') {  // if you got anything else but a carriage return character,
            currentLine += c;      // add it to the end of the currentLine
        }
    }
}


uint8_t WebServer::respond()
{   
    this->client = server.available();   // Listen for incoming clients
    if(this->client)
    {
        if(this-client.connected() && this->client.available()){
             this->check_request();
        }
    }
}

int8_t WebServer::GET_actions()
{
    if (this->header.indexOf("GET /startMeasurement") >= 0) {
        this->post_start_measurement_response();
        this->is_start_measurement_triggered = true;
        return SUCCESS;
    }
    else if (this->header.indexOf("GET /startCalibration") >= 0) 
    {
        this->post_start_calibration_response();
        this->is_start_calibration_triggered = true;
        return SUCCESS;
    }
    else if (this->header.indexOf("GET /measure") >= 0) {
        this->post_permeability_value();
        return SUCCESS;
    }
    else if (this->header.indexOf("GET /") >= 0) {
        this->displayPage();
        return SUCCESS;
    }
    
}

uint8_t WebServer::displayPage()
{   
    // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
    // and a content-type so the client knows what's coming, then a blank line:
    this->client.println("HTTP/1.1 200 OK");
    this->client.println("Content-type:text/html");
    this->client.println("Connection: keep-alive");
    this->client.println();

    // Display the HTML web page
    this->client.println("<!DOCTYPE html><html>");
    this->client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
    this->client.println("<link rel=\"icon\" href=\"data:,\">");

    // Web Page Heading
    this->client.println("<body>ESP32 Web Server\n\r");
    this->client.println("Status: " + this->measurementStatus);
    this->client.println("</body></html>");

    // The HTTP response ends with another blank line
    this->client.println();
    this->client.stop();
    return 1;
}

void WebServer::post_permeability_value()
{   
    Serial.println("---post_permeability_value---");
    String json = "";
    this->client.println("HTTP/1.1 200 OK");
    this->client.println("Content-type:text/html");
    this->client.println("Connection: keep-alive");
    this->client.println();

    // Display the HTML web page
    this->client.println("<!DOCTYPE html><html>");
    this->client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
    this->client.println("<link rel=\"icon\" href=\"data:,\">");

    json += "{";
    json += "\"PermValue\": \"" + this->perm_value          + "\",";
    json += "\"ProbeTime\": \"" + this->moving_time         + "\",";
    json += "\"Direction\": \"" + this->direction           + "\",";
    json += "\"Status\": \""    + this->measurementStatus   + "\"}";

    Serial.println(json);

    this->client.println("<body>" + json);
    this->client.println("</body></html>");
    // The HTTP response ends with another blank line
    this->client.println();
    this->client.stop();
    Serial.println("client stopped");
}


void WebServer::post_start_measurement_response()
{
    this->client.println("HTTP/1.1 200 OK");
    this->client.println("Content-type:text/html");
    this->client.println("Connection: keep-alive");
    this->client.println();
    // Display the HTML web page
    this->client.println("<!DOCTYPE html><html>");
    this->client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
    this->client.println("<link rel=\"icon\" href=\"data:,\">");
    this->client.println("<body>");
    this->client.println("Status: started");
    this->client.println("To see actual probe go to: /measure");
    this->client.println("</body></html>");
    // The HTTP response ends with another blank line
    this->client.println();
    this->client.stop();
}

void WebServer::post_start_calibration_response()
{
    this->client.println("HTTP/1.1 200 OK");
    this->client.println("Content-type:text/html");
    this->client.println("Connection: keep-alive");
    this->client.println();
    // Display the HTML web page
    this->client.println("<!DOCTYPE html><html>");
    this->client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
    this->client.println("<link rel=\"icon\" href=\"data:,\">");
    this->client.println("<body>");
    this->client.println("Status: started");
    this->client.println("To see actual probe go to: /measure");
    this->client.println("</body></html>");
    // The HTTP response ends with another blank line
    this->client.println();
    this->client.stop();
}

bool WebServer::is_request_measurement()
{
    if (!this->is_start_measurement_triggered) 
        return false;
    this->is_start_measurement_triggered = false;
    return true;
}

bool WebServer::is_request_calibration()
{
    if (!this->is_start_calibration_triggered) 
        return false;
    this->is_start_calibration_triggered = false;
    return true;
}

void WebServer::set_measurement_status(String status)
{
    this->measurementStatus = status;
}

void WebServer::set_direction(String dir)
{
    this->direction = dir;
}

void WebServer::set_moving_time(unsigned long moving_time)
{   
    this->moving_time = moving_time/1000;
}

void WebServer::set_perm_value(double perm_value)
{
    this->perm_value = perm_value;
}


// String permMeasure = String(patch::to_string(perm_value).c_str());
// String moveTime = String(patch::to_string(moving_time/1000.0).c_str());