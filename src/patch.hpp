#ifndef __MYPATCH_H__
#define __MYPATCH_H__
#include <string>
#include <iostream>
#include <sstream>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}



#endif
