#ifndef __PERMEABILITYMEASURE_H__
#define __PERMEABILITYMEASURE_H__

#include <Arduino.h>
#include <stdint.h>
#include <vector>

using namespace std;

class Permeability
{
private:
    static uint8_t measureLC_pin;
    static uint8_t chargeLC_pin;
    static uint8_t sample_count;
    static double capacitance;
    static volatile double inductance;
    static volatile double base_inductance;

public:
    static void init();
    static void charge_lc_circuit(int charge_time, int gap_time);
    static void charge_lc_circuit();
    static vector<double> gatherSamples(uint8_t sample_count);
    static unsigned long measurePulse();
    static double meanValue(vector<double> samples);
    static double mesure();
    static void set_base_inductance();
};
#endif
