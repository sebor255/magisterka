#ifndef __LCDDISPLAY_H__
#define __LCDDISPLAY_H__

#include <Arduino.h>
#include <stdint.h>
#include <LiquidCrystal.h>
#include <string>

using namespace std;

class LCD
{
public:
    static string upper_line;
    static string lower_line;
    static const uint8_t lcd_rs_pin;
    static const uint8_t lcd_enable_pin;
    static const uint8_t lcd_d4_pin;
    static const uint8_t lcd_d5_pin;
    static const uint8_t lcd_d6_pin;
    static const uint8_t lcd_d7_pin;

    // According to Arduino:
    // LiquidCrystal(uint8_t rs,            - A5
    //                  uint8_t enable,     - A4
    //                  uint8_t d7,         - A3
    //                  uint8_t d6,         - A2
    //                  uint8_t d5,         - A1
    //                  uint8_t d4);        - A0
    //
    // Wroom-32 pinout:
    // rs       --  P34
    // enable   --  P35
    // D4       --  P32
    // D5       --  P33
    // D6       --  P25
    // D7       --  P26
    static LiquidCrystal lcd;

    static void init();
    static void print_init_screen();
    static void print_upper_line(string to_print);
    static void print_lower_line(string to_print);

};

#endif