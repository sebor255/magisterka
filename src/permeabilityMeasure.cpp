#include "permeabilityMeasure.hpp"
using namespace std;
#define NUMBER_OF_TURNS 23


uint8_t         Permeability::measureLC_pin     = 2;
uint8_t         Permeability::chargeLC_pin      = 15;
uint8_t         Permeability::sample_count      = 32;
double          Permeability::capacitance       = 100E-9;
volatile double Permeability::inductance        = 0;
volatile double Permeability::base_inductance   = 0;

void Permeability::init()
{
    pinMode(measureLC_pin, INPUT);
    pinMode(chargeLC_pin, OUTPUT);
}

void Permeability::charge_lc_circuit(int charge_time, int gap_time)
{
    digitalWrite(chargeLC_pin, HIGH);
    delayMicroseconds(charge_time);
    digitalWrite(chargeLC_pin, LOW);
    delayMicroseconds(gap_time);
}

void Permeability::charge_lc_circuit()
{
    charge_lc_circuit(150,1);
}

unsigned long Permeability::measurePulse()
{
    // wait max 10 000us for pulse; return: pulse length measured in us
    // return is pulse in [us]  
    return pulseIn(measureLC_pin, LOW, 10000);
}

vector<double> Permeability::gatherSamples(uint8_t sample_count)
{   
    vector<double> samples;
    for(uint8_t i=0;i<sample_count; i++)
    {
        charge_lc_circuit();
        // pulse long is in micro seconds; and the measured pulse is only a half of the sinus 
        // so it has to be multiplied by 2
        double frequency = 1.0e6/(2*measurePulse());
        samples.push_back(frequency);
        delay(20);
    }
    return samples;

}

double Permeability::meanValue(vector<double> samples)
{   
    double meanVal = 0;
    for(uint8_t i=0;i<samples.size();i++)
    {
        meanVal += samples.at(i);
    }
    meanVal /=samples.size();
    return meanVal;
}

double Permeability::mesure()
{   
    static const double area_of_inductor_m2 = 0.001590431281; //1590.43128087983282*1e-6;
    static const double inductance_of_vaccum = 1.25663706212*1e-6;
    static const uint8_t inductor_turns_square = NUMBER_OF_TURNS*NUMBER_OF_TURNS;
    static const double inductor_length_m = 1e-2;
    
    double frequency = meanValue(gatherSamples(sample_count));
    //inductance out of LC circuit:
    inductance = (1. / (capacitance * frequency * frequency * PI*PI* 4.));
    Serial.print("Inductance:");
    Serial.println(inductance*1000*1000);
    //relative permeability out of inductance
    //return (inductance*inductor_length_m)/(inductor_turns_square*area_of_inductor_m2*inductance_of_vaccum);
    double a = (inductance - base_inductance)*inductor_length_m;
    double b = inductor_turns_square*area_of_inductor_m2*inductance_of_vaccum;
    return a/b + 1;
}
    

void Permeability::set_base_inductance()
{
    double frequency = meanValue(gatherSamples(sample_count));

    Serial.print("frequency = ");
    Serial.println(frequency);
    //inductance out of LC circuit:
    base_inductance = (1. / (capacitance * frequency * frequency * PI*PI* 4.));
    Serial.print("base_inductance = ");
    Serial.println(base_inductance*1000*1000);
}