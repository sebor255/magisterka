#include "motorController.hpp"


MotorControl::MotorControl(uint8_t left_pwm_pin, uint8_t right_pwm_pin, 
                //uint8_t left_limit_pin, uint8_t righ_limit_pin, 
                uint8_t pwm_channel_left, uint8_t pwm_channel_right,
                uint32_t frequency, uint8_t duty_resolution )
{
    this->left_pwm_pin = left_pwm_pin;
    this->right_pwm_pin = right_pwm_pin;
    this->left_limit_pin = left_limit_pin;
    this->righ_limit_pin = righ_limit_pin;
    this->pwm_channel_left = pwm_channel_left;
    this->pwm_channel_right = pwm_channel_right;
    this->frequency = frequency;
    this->duty_resolution = duty_resolution;
    this->duty_cycle = 0;

    // set pin modes
    pinMode(this->left_pwm_pin, OUTPUT);
    pinMode(this->right_pwm_pin, OUTPUT);

    // setup pwmpwm_channel_rev
    ledcSetup(this->pwm_channel_left, this->frequency, this->duty_resolution);
    ledcSetup(this->pwm_channel_right, this->frequency, this->duty_resolution);
    ledcAttachPin(this->left_pwm_pin, this->pwm_channel_left);
    ledcAttachPin(this->right_pwm_pin, this->pwm_channel_right);

    //turn off motors
    ledcWrite(this->pwm_channel_left, this->duty_cycle);
    ledcWrite(this->pwm_channel_right, this->duty_cycle);
}

void MotorControl::startLeft(uint16_t duty_cycle=-1){
    this->stopRight();
    if(duty_cycle!=-1){
        this->duty_cycle = duty_cycle;
    }
    ledcWrite(this->pwm_channel_left, this->duty_cycle);
}

void MotorControl::stopLeft(void){
    ledcWrite(this->pwm_channel_left, 0);
}

void MotorControl::startRight(uint16_t duty_cycle=-1){
    this->stopLeft();
    if(duty_cycle!=-1){
        this->duty_cycle = duty_cycle;
    }
    ledcWrite(this->pwm_channel_right, this->duty_cycle);
}

void MotorControl::stopRight(void){
    ledcWrite(this->pwm_channel_right, 0);
}

void MotorControl::test(void){
    Serial.print("Frequency: ");
    Serial.println(this->frequency);
    this->startLeft(120);
    Serial.print("DutyCycle: ");
    Serial.println((float)(this->duty_cycle/255));
    delay(2000);
    this->startLeft(255);
    Serial.print("DutyCycle: ");
    Serial.println((float)(this->duty_cycle/255));
    delay(2000);
    this->startRight(120);
    Serial.print("DutyCycle: ");
    Serial.println((float)(this->duty_cycle/255));
    delay(2000);
    this->startRight(255);
    Serial.print("DutyCycle: ");
    Serial.println((float)(this->duty_cycle/255));
    delay(2000);
    this->stopRight();
    delay(2000);
}
