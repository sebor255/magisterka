#include "wifiConnection.hpp"

extern const char* targetSsid;
extern const char* targetPassword;
IPAddress WifiConnection::ipaddress;

void WifiConnection::init()
{
    // Set WiFi to station mode and disconnect from an AP if it was previously connected
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
}


uint8_t WifiConnection::searchNetworks()
{
    Serial.println("Network scanning...");

    // WiFi.scanNetworks will return the number of networks found
    int n = WiFi.scanNetworks();
    Serial.print("Scan done: ");
    if (n == 0) {
        Serial.print("no networks found!");
        return 0;
    } else {
        Serial.print(n);
        Serial.print(" networks found:\n");
        for (int i = 0; i < n; ++i) {
        // Print SSID and RSSI for each network found
        Serial.print(i + 1);
        Serial.print(": ");
        Serial.print(WiFi.SSID(i));
        Serial.print(" (");
        Serial.print(WiFi.RSSI(i));
        Serial.print(")");
        Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");

        }
    }
    Serial.println("\n\r");
    return 1;
}

uint8_t WifiConnection::connectToNetwork()
{   
    int period = 100;
    unsigned long time_last_loop = 0;
    WiFi.mode(WIFI_STA);
    WiFi.begin(targetSsid, targetPassword);
    Serial.print("Connecting to WiFi");
    while (WiFi.status() != WL_CONNECTED) {
        if(millis() >= time_last_loop + period){
            time_last_loop += millis();
            Serial.print('.');
        }
    }
    Serial.println();
    Serial.print("Device address: ");
    ipaddress = WiFi.localIP();
    Serial.println(ipaddress);
}
