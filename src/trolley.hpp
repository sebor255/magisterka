#ifndef __TROLLEY_H__
#define __TROLLEY_H__
#include "motorController.hpp"
#include <Arduino.h>
#include "scheduler.hpp"
#include "lcdDisplay.hpp"
#include "patch.hpp"

const uint8_t motor_speed = 180;

class Trolley{   
    static bool areTasksEnabled;
public:
    static bool is_positioning;
    static bool is_positioned;

    static bool is_calibrating;
    static bool is_calibrated;

    static unsigned long time_start_motor_up;
    static unsigned long time_start_motor_down;
    static unsigned long time_moving_motor_up_ms;
    static unsigned long time_moving_motor_down_ms;

    static bool is_move_one_cycle;
    static bool is_one_cycle_finished;

    static bool is_moving_up;
    static bool is_moving_down;

    static const uint16_t height_mm;
    static float down_speed_mm_per_s;
    static float up_speed_mm_per_s;

    static const uint8_t trolley_up_pin;
    static const uint8_t trolley_down_pin;
    static const uint8_t trolley_up_limit_pin;
    static const uint8_t trolley_down_limit_pin;

    static void init();
    static void calibration();
    static void moveOneCycle();
    static void moveUp();
    static void moveUpFor(unsigned long time_ms);
    static void moveDown();
    static void moveDownFor(unsigned long time_ms);
    static bool position_to_very_top();
};
#endif