#ifndef __MOTORCONTROLLER_H__
#define __MOTORCONTROLLER_H__

#include <Arduino.h>
#include <stdint.h>

/*
IBT-2 Motor Control Board driven by Arduino.
 
Speed and direction controlled by a potentiometer attached to analog input 0.
One side pin of the potentiometer (either one) to ground; the other side pin to +5V
 
Connection to the IBT-2 board:
IBT-2 pin 1 (RPWM) to Arduino pin 5(PWM)
IBT-2 pin 2 (LPWM) to Arduino pin 6(PWM) 
IBT-2 pins 3 (R_EN), 4 (L_EN), 7 (VCC) to Arduino 5V pin
IBT-2 pin 8 (GND) to Arduino GND
IBT-2 pins 5 (R_IS) and 6 (L_IS) not connected
*/

class MotorControl{
private: 
    uint8_t left_pwm_pin;
    uint8_t right_pwm_pin;
    uint8_t left_limit_pin;
    uint8_t righ_limit_pin;
    uint8_t duty_cycle;
    uint8_t pwm_channel_left;
    uint8_t pwm_channel_right;
    uint32_t frequency;
    uint8_t duty_resolution;
public:
    MotorControl(uint8_t left_pwm_pin, uint8_t right_pwm_pin, 
                //uint8_t left_limit_pin=0, uint8_t righ_limit_pin=0, 
                uint8_t pwm_channel_left=0, uint8_t pwm_channel_right=1,
                uint32_t frequency=30000, uint8_t duty_resolution = 8);
    void startLeft(uint16_t duty_cycle);
    void stopLeft(void);
    void startRight(uint16_t duty_cycle);
    void stopRight(void);
    void test(void);
};
#endif
