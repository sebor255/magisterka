#ifndef __MEASUREMENT_H__
#define __MEASUREMENT_H__

#include "trolley.hpp"
#include "permeabilityMeasure.hpp"
#include "lcdDisplay.hpp"
#include "scheduler.hpp"
#include "patch.hpp"

#include <Arduino.h>
#include <stdint.h>

using namespace std;

class Measurement{

public:
    static void startMeasuring();
    
};
#endif
