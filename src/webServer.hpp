#ifndef __WEBSERVER_H__
#define __WEBSERVER_H__

#include <stdint.h>
#include <string.h>
#include "WiFi.h"


class WebServer
{
private:
    // Set web server port number to 80
    WiFiServer server;

    // Client connected to the server
    WiFiClient client;

    // Variable to store the HTTP request
    String header;

    // Current time
    unsigned long currentTime;
    // Previous time
    unsigned long previousTime; 
    // Define timeout time in milliseconds (example: 2000ms = 2s)
    const unsigned long timeoutTime = 100;

    // // Needed for led steering
    // String output26State;
    // String output27State;
    
    // const int output26 = 26;
    // const int output27 = 27;

    // for measurement steering
    bool is_start_measurement_triggered = false;
    bool is_start_calibration_triggered = false;

    String measurementStatus = "none";
    String direction = "None";
    String moving_time = "None"; 
    String perm_value = "None"; 
    

    // Private methods
    uint8_t displayPage();
    void displayPage_gpio26Button();
    void displayPage_gpio27Button();

    int8_t GET_actions();
    void gpio_on(uint8_t gpioNo);
    void gpio_off(uint8_t gpioNo);

    void check_request();
    void post_start_measurement_response();
    void post_start_calibration_response();

public:
    void set_direction(String dir);
    void set_moving_time(unsigned long moving_time);
    void set_perm_value(double perm_value);
    void set_measurement_status(String status);
    bool is_request_measurement();
    bool is_request_calibration();
    WebServer(uint16_t portToListen);
    void post_permeability_value();
    uint8_t wait_for_client();
    uint8_t respond();
};

#endif