#include "lcdDisplay.hpp"
#include <Arduino.h>

string LCD::upper_line = "abcdefghij";;
string LCD::lower_line = "0123456789";
const uint8_t LCD::lcd_rs_pin = 23;
const uint8_t LCD::lcd_enable_pin = 22;
const uint8_t LCD::lcd_d4_pin = 32;
const uint8_t LCD::lcd_d5_pin = 33;
const uint8_t LCD::lcd_d6_pin = 25;
const uint8_t LCD::lcd_d7_pin = 26;
LiquidCrystal LCD::lcd = LiquidCrystal(LCD::lcd_rs_pin, LCD::lcd_enable_pin, LCD::lcd_d4_pin, 
                            LCD::lcd_d5_pin, LCD::lcd_d6_pin, LCD::lcd_d7_pin);


void LCD::init()
{
    int colums=16, rows=2; 
    lcd.begin(colums,rows);
}

void LCD::print_init_screen()
{
    print_upper_line("Permeability me-");
    print_lower_line("ter using ESP32 ");
}

void LCD::print_upper_line(string to_print)
{   
    lcd.clear();
    upper_line = to_print;
    lcd.setCursor(0, 0);
    lcd.print(upper_line.c_str());
    lcd.setCursor(0, 1);
    lcd.print(lower_line.c_str());
}

void LCD::print_lower_line(string to_print)
{
    lcd.clear();
    lower_line = to_print;
    lcd.setCursor(0, 0);
    lcd.print(upper_line.c_str());
    lcd.setCursor(0, 1);
    lcd.print(lower_line.c_str());
}
    