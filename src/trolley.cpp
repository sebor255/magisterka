#include "trolley.hpp"

// interruptions handling
const uint32_t debounce_time = 1000;
volatile bool is_up_limit_reached = true; // assumption is that the trolley ends
                                          // and start in the upper limit position, so it should
                                          // always start to go down to not destroy the construction
volatile bool is_down_limit_reached = false;
volatile long last_interrupt_time = 0;

// interruption callbacks
void trolley_up_limit_rising()
{
    long timeNow = millis();
    if (timeNow - last_interrupt_time > debounce_time)
    {
        Serial.println("interrupt from: trolley_up_limit_rising");
        last_interrupt_time = timeNow;
        is_up_limit_reached = true;
    }
}

void trolley_down_limit_rising()
{   
    long timeNow = millis();
    if (millis() - last_interrupt_time > debounce_time)
    {
        Serial.println("interrupt from: trolley_down_limit_rising");
        last_interrupt_time = timeNow;
        is_down_limit_reached = true;
    }
}

//global variables
MotorControl m1 = MotorControl(Trolley::trolley_up_pin, Trolley::trolley_down_pin);

// static variables initialization
bool Trolley::areTasksEnabled = false;

bool Trolley::is_positioning = false;
bool Trolley::is_positioned = false;

bool Trolley::is_calibrating = true;
bool Trolley::is_calibrated = false;

unsigned long Trolley::time_start_motor_up = 0;
unsigned long Trolley::time_start_motor_down= 0;
unsigned long Trolley::time_moving_motor_up_ms= 0;
unsigned long Trolley::time_moving_motor_down_ms= 0;

bool Trolley::is_move_one_cycle = false;
bool Trolley::is_one_cycle_finished = false;
bool Trolley::is_moving_up = false;
bool Trolley::is_moving_down = false;

const uint16_t Trolley::height_mm = 323;
float Trolley::down_speed_mm_per_s = 0;
float Trolley::up_speed_mm_per_s = 0;
const uint8_t Trolley::trolley_up_pin = 13;
const uint8_t Trolley::trolley_down_pin = 12;
const uint8_t Trolley::trolley_up_limit_pin = 14;
const uint8_t Trolley::trolley_down_limit_pin = 27;

// task functions prototypes
void mUpStopDelay();
void mDownStopDelay();
void checkUpLimit();
void checkDownLimit();
void calibration();
void stopTrolleyAfterTime();

// tasks
Task tCalibration(500, TASK_FOREVER, &calibration);
Task tCheckUpLimit(100, TASK_FOREVER, &checkUpLimit);
Task tCheckDownLimit(100, TASK_FOREVER, &checkDownLimit);
Task tStopTrolley(TASK_IMMEDIATE, TASK_ONCE, &stopTrolleyAfterTime);

// task functions
void stopTrolleyAfterTime()
{
    m1.stopLeft();
    m1.stopRight();
}

void calibration()
{
    if (Trolley::is_calibrating && !Trolley::is_move_one_cycle)
    {   
        Serial.println("calibration movement ended");
        Trolley::up_speed_mm_per_s = Trolley::height_mm/(Trolley::time_moving_motor_up_ms *1e-3);
        Trolley::down_speed_mm_per_s = Trolley::height_mm/(Trolley::time_moving_motor_down_ms *1e-3);

        LCD::print_upper_line("Up: " + patch::to_string((double)Trolley::up_speed_mm_per_s) + " mm/s");
        LCD::print_lower_line("Dwn: " + patch::to_string((double)Trolley::down_speed_mm_per_s) + " mm/s");
        Serial.print("up speed: ");
        Serial.println(Trolley::up_speed_mm_per_s);
        Serial.print("down speed: ");
        Serial.println(Trolley::down_speed_mm_per_s);

        runner.deleteTask(tCalibration);
        tCalibration.disable();

        Trolley::is_calibrating = false;
        Trolley::is_calibrated = true;
    }   
}


void checkUpLimit()
{   
    //Serial.println("checking up limit");
    if (HIGH == digitalRead(Trolley::trolley_up_limit_pin))
    {   
        Trolley::time_moving_motor_up_ms = millis() - Trolley::time_start_motor_up;
        m1.stopLeft();
        Trolley::is_moving_up = false;
        Serial.print("Up moving time [ms]: ");
        Serial.println(Trolley::time_moving_motor_up_ms);
        if(Trolley::is_move_one_cycle)
        {
            Serial.println("move one cycle finished");
            Trolley::is_one_cycle_finished = true;
            Trolley::is_move_one_cycle = false;
        }   
        runner.deleteTask(tCheckUpLimit);
        tCheckUpLimit.disable();
    }
}

void checkDownLimit()
{   
    //Serial.println("checking down limit");
    if (HIGH == digitalRead(Trolley::trolley_down_limit_pin))
    {
        Trolley::time_moving_motor_down_ms = millis() - Trolley::time_start_motor_down;
        m1.stopRight();
        Trolley::is_moving_down = false;
        Serial.print("Down moving time [ms]: ");
        Serial.println(Trolley::time_moving_motor_down_ms);
        if (Trolley::is_move_one_cycle)
        {
            Trolley::moveUp();
        }
        runner.deleteTask(tCheckDownLimit);
        tCheckDownLimit.disable();
    }
}

// class functions
void Trolley::init()
{
    pinMode(trolley_up_limit_pin, INPUT_PULLDOWN);
    pinMode(trolley_down_limit_pin, INPUT_PULLDOWN);

    attachInterrupt(digitalPinToInterrupt(trolley_up_limit_pin), trolley_up_limit_rising, RISING);
    attachInterrupt(digitalPinToInterrupt(trolley_down_limit_pin), trolley_down_limit_rising, RISING);
    runner.addTask(tStopTrolley);
}

void Trolley::moveUp()
{   
    if (LOW == digitalRead(trolley_up_limit_pin))
    {
        Serial.println("moving up");
        m1.startLeft(motor_speed);

        is_down_limit_reached = false;
        time_moving_motor_up_ms = 0;
        time_start_motor_up = millis();

        runner.addTask(tCheckUpLimit);
        tCheckUpLimit.setInterval(100);
        tCheckUpLimit.enable();
        is_moving_up = true;
    }
}

void Trolley::moveDown()
{   
    if (LOW == digitalRead(trolley_down_limit_pin))
    {
        Serial.println("moving down");
        m1.startRight(motor_speed);

        is_up_limit_reached = false;
        time_moving_motor_down_ms = 0;
        time_start_motor_down = millis();

        runner.addTask(tCheckDownLimit);
        tCheckDownLimit.setInterval(100);
        tCheckDownLimit.enable();
        is_moving_down = true;
    }
}


void Trolley::moveOneCycle()
{   
    is_move_one_cycle = true;
    is_one_cycle_finished = false;
    moveDown();   
}

bool Trolley::position_to_very_top()
{   
    Serial.println("Positioning to very top");
    LCD::print_upper_line("Positioning");
    LCD::print_lower_line("to very top");
    if (HIGH == digitalRead(Trolley::trolley_up_limit_pin))
    {
        // up limit already reached
        is_positioning = false;
        is_positioned = true;
    }
    else
    {
        // down limit reached, or the trolley is somewhere between top and down - move to the top. 
        is_positioning = true;
        is_positioned = false;
        moveUp();
        
    }
    return is_positioned;
}

void Trolley::calibration()
{   
    is_calibrating = true;
    is_calibrated = false;
    
    Serial.println("calibration started");
    LCD::print_upper_line("calibration");
    LCD::print_lower_line("started");

    Trolley::is_calibrating = true;
    Trolley::moveOneCycle();  

    //tCalibrate awaits for oneCycle to be finished and then starts proper calibration
    runner.addTask(tCalibration);
    tCalibration.enable();
    
}

void setUpTrolleyStop(unsigned long time_ms)
{
    tStopTrolley.setInterval(time_ms);
    if (!tStopTrolley.isEnabled()) 
    {
        tStopTrolley.enable();
    }else
    {
        tStopTrolley.restart();
    }
}

void Trolley::moveUpFor(unsigned long time_ms)
{
    setUpTrolleyStop(time_ms);
    moveUp();
}

void Trolley::moveDownFor(unsigned long time_ms)
{
    setUpTrolleyStop(time_ms);
    moveDown();
}

