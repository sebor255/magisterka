#include "src\wifiConnection.hpp"
#include "src\measurement.hpp"
#include "src\lcdDisplay.hpp"
#include "src\webServer.hpp"
#include "src\trolley.hpp"
#include <string>
#include "esp_task_wdt.h"
#include "src\permeabilityMeasure.hpp"
#include "src\patch.hpp"

#include "src\scheduler.hpp"
void mainProgram();
void initModules();
void permMeasure();

volatile double permValue = 0;

Task tMainProgram(333, TASK_FOREVER, &mainProgram);
Task tPermMeasure(TASK_IMMEDIATE, TASK_ONCE, &permMeasure);

Scheduler runner;

int period = 5000;
unsigned long time_now = 0;
WebServer *webServer;

void setup()
{
  initModules();
  LCD::print_init_screen();
  WifiConnection::searchNetworks();
  WifiConnection::connectToNetwork();
  // web server has to be created after the esp32 obtains the IP
  webServer = new WebServer(80);
  webServer->wait_for_client();

  // starting task scheduler and assigned tasks 
  runner.startNow();
  Trolley::position_to_very_top();
  if (!Trolley::is_positioned) webServer->set_measurement_status("positioning");
  runner.addTask(tMainProgram);
  tMainProgram.enable();
}

void loop()
{
  runner.execute();
}

bool measuring = false;
// MAIN PROGRAM //
void mainProgram()
{
  webServer->respond();
  if(Trolley::is_positioned && webServer->is_request_calibration())
  {
    webServer->set_measurement_status("calibration");
    Trolley::calibration();
  }
  else if(Trolley::is_positioned && webServer->is_request_measurement())
  { 
    webServer->set_measurement_status("measuring");
    Serial.println("starting measurement");
    measuring = true;
    
    Permeability::set_base_inductance();
    runner.addTask(tPermMeasure);
    tPermMeasure.enable();
    Trolley::moveOneCycle();
    
  }
  else if (measuring)
  {
    if(Trolley::is_one_cycle_finished)
    {
      webServer->set_measurement_status("waiting");

      tPermMeasure.disable();
      runner.deleteTask(tPermMeasure);
      LCD::print_upper_line("measuring has");
      LCD::print_lower_line("been ended");
      measuring = false;
      
    }
    tPermMeasure.restartDelayed(0);
  }
}

void setHttpValues()
{
  static unsigned long moving_time;
  static String moving_dir;
  
  if(Trolley::is_moving_up)
  {
    moving_dir = "Up";
    moving_time = millis() - Trolley::time_start_motor_up;
  }else if (Trolley::is_moving_down)
  {
    moving_dir = "Down";
    moving_time = millis() - Trolley::time_start_motor_down;
  }
  webServer->set_perm_value(permValue);
  webServer->set_direction(moving_dir);
  webServer->set_moving_time(moving_time);

}

void initModules()
{
  esp_task_wdt_reset();
  Serial.end();
  Serial.begin(115200);
  WifiConnection::init();
  LCD::init();
  Trolley::init();
  Permeability::init();
}

void permMeasure()
{
  permValue = Permeability::mesure();
  LCD::print_upper_line("permeability:");
  LCD::print_lower_line(patch::to_string(permValue));
  setHttpValues();
}