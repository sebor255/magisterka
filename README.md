## Setup

To set up working repo you need to do the following:

1. Download Arduino IDE and install it in the default location
2. Download and install VSC
3. Download plugin Arduino to the VSC in the plugin tab of the visual studio
4. Open folder "Magisterka" in the VSC. 
5. Main program which has to be compiled and uploaded to the ESP32 is main.ino